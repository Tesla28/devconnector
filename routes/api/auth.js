const { getRounds } = require('bcryptjs');
const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth')
const User = require('../../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('config');
const { check, validationResult } = require('express-validator/check');

// @route   GET api/auth
// @desc    Auth route
// @access  Public
router.get('/', auth,  async (req, res) => {
    try{
        const user = await User.findById(req.user.id).select('-password');
        res.json(user); 
    }catch(err){
        console.error(err.message)
        res.status(500).json({ msg: 'Server error'});
    }
});

// @route   POST api/auth
// @desc    Authenticate user & get token
// @access  Public
router.post('/', 
[
    check('email', 'Please enter a valid email').isEmail(),
    check('password', 'Password is required')
        .exists()
],
async (req, res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const { email, password } = req.body;

    try{
            
    // See if the user exists
    let user = await User.findOne({ email });
    if(!user) {
        return res.status(400)
                .json({errors: [{ msg: 'Invalid credentials' }]});
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if(!isMatch) {
        return res.status(400)
                .json({errors: [{ msg: 'Invalid credentials' }]});
    }

    // Return jsonwebtoken
    const payload = {
        user: {
            id: user.id
        }
    }

    jwt.sign(
        payload, 
        config.get('jwtToken'),
        {expiresIn : 360000},
        (err, token) => {
            if(err) throw err;
            res.status(200).json({ 
                message: 'Login Success',
                name: user.name,
                email: user.email,
                token })
        })

    }catch(err){
        console.error(err.message);
        res.status(500).send('Server error');

    }
});

module.exports = router;